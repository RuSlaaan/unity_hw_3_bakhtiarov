﻿using System.Collections.Generic;
using Base;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
  private static GameState state;
  enum GameState
  {
    GAME,
    VICTORY,
    DEFEAT
  }
  public GameObject player_go;
  public GameObject exit_go;
  public GameObject exit_pulse_go;
  public GameObject marker_go;
  public Text lefttime_ui;
  public GameObject arrow_go;
  public GameObject camera_go;
  public GameObject vfx_step;
  public AudioClip sfx_step;
  public AudioClip ambient;
  public  GameObject portal;
   public  GameObject portal1;
    public GameObject vxod;
  
  public RawImage gamepad_base_go;
  public RawImage gamepad_stick_go;

  static float start_game_stamp;
  float game_time = 20;
  
  float spawn_markers_stamp;
  float spawn_markers_cooldown = 0.5f;
  
  Exit exit;
  Player player;
    Marker first_marker; 
  Color exit_color;
  PlayerFollowing player_following;
  Animator lefttime_animator;
  AudioSource ambient_source;
 public  static Animator portal_animator;
  
  void Start()
  {
    state = GameState.GAME;
    marker_go.SetActive(false);
    
    exit_color = GetRandomColor(1);

    Cube.vfx_step = vfx_step;
    Cube.sfx_step = sfx_step;
    
    first_marker = CreateMarker();
    first_marker.Init(exit_color, first_marker.transform.position, Vector3.zero, this);
    
    PlayerInput input = player_go.AddComponent<PlayerInput>();
    input.Init(gamepad_base_go, gamepad_stick_go);
    
    player = player_go.AddComponent<Player>();
    
    exit = exit_go.AddComponent<Exit>();
    exit.Init(exit_color, player);
    
    player_following = camera_go.AddComponent<PlayerFollowing>();
    player_following.Init(player.gameObject);


        portal_animator = portal.GetComponent<Animator>();
        portal_animator.SetBool("open", false);
        portal_animator.SetBool("On", false);

        lefttime_animator = lefttime_ui.gameObject.GetComponent<Animator>();
    lefttime_animator.SetBool("Alert", false);
    lefttime_animator.SetTrigger("Cancel");
    
    Color pulse_color = exit_color;
    pulse_color.a = 0.25f;
    ColorizeGo(exit_pulse_go, pulse_color);

    ambient_source = gameObject.AddComponent<AudioSource>();
    ambient_source.clip = ambient;
    ambient_source.loop = true;
    ambient_source.Play();
    
    start_game_stamp = Time.time;
  }

  void ColorizeGo(GameObject go, Color color)
  {
    Renderer renderer = go.GetComponent<Renderer>();
    var tempMaterial = new Material(renderer.sharedMaterial);
    tempMaterial.color = color;
    tempMaterial.SetColor("_EmissionColor", Mathf.LinearToGammaSpace (0.02f)*color);
    renderer.sharedMaterial = tempMaterial;
  }
  
  float GetLeftTime()
  {
    return Mathf.Max(0, start_game_stamp + game_time - Time.time);
  }

  Marker CreateMarker()
  {
    GameObject marker = Instantiate(marker_go);
    marker.SetActive(true);
    return marker.AddComponent<Marker>(); 
  }

  float exit_arrow_radius = 20;
  float spawn_radius = 30;
  Rect spawn_rect = Rect.zero;
    public static int kk;
  void Update()
  {

    if (state == GameState.DEFEAT)
    {
      lefttime_ui.text = "";
      arrow_go.SetActive(false);
      exit_pulse_go.SetActive(false);
      ambient_source.volume = 0;
      return;
    }
       

        if (metka == true)
        {

            lefttime_ui.text = "";
            arrow_go.SetActive(false);
            exit_pulse_go.SetActive(false);
            ambient_source.volume = 0;
            
            portal1.SetActive(true);
            Marker.destroy();

            return;

        }


        if (GetLeftTime() == 0)
            GameOver(false);
        else
            lefttime_animator.SetBool("Alert", GetLeftTime() < 5);

        ambient_source.volume = player.activity;
    
    Vector3 player_pos = player.transform.position;
    Vector3 exit_pos = exit.transform.position;
    Vector3 center_screen_pos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width/2, Screen.height/2)) - player_following.offset;
    
    lefttime_ui.text = "Left Time: " + string.Format("{0:0.00}", GetLeftTime());

    exit_pulse_go.SetActive(exit.PlayerIsReadyForExit());
    arrow_go.SetActive((exit_pos-center_screen_pos).magnitude > exit_arrow_radius);

    Vector3 player_screen_pos = Camera.main.WorldToScreenPoint(player_pos);
    Vector3 exit_screen_pos = Camera.main.WorldToScreenPoint(exit_pos);
    player_screen_pos.z = 0;
      exit_screen_pos.z = 0;
    Vector3 dir_to_exit = exit_screen_pos - player_screen_pos;
    float delta = 40;
    Vector3 exit_arrow_screen_pos = exit_screen_pos - dir_to_exit.normalized*delta;
    
    arrow_go.transform.position = new Vector3(Mathf.Clamp(exit_arrow_screen_pos.x, delta, Screen.width-delta), Mathf.Clamp(exit_arrow_screen_pos.y, delta, Screen.height-delta), 0);
    arrow_go.transform.Find("open").right = (exit_screen_pos - arrow_go.transform.position).normalized;
    arrow_go.transform.Find("open").gameObject.SetActive(exit.PlayerIsReadyForExit());
    arrow_go.transform.Find("locked").gameObject.SetActive(!exit.PlayerIsReadyForExit());
    arrow_go.transform.Find("color").GetComponent<RawImage>().color = exit_color;

    if (first_marker != null)
    {
      start_game_stamp = Time.time;
      return;
    }
    
    if (spawn_markers_stamp + spawn_markers_cooldown > Time.time)
      return;
    
    spawn_markers_stamp = Time.time;
    spawn_rect.position = new Vector2(player_pos.x - spawn_radius, player_pos.z - spawn_radius);
    spawn_rect.size = new Vector2(spawn_radius*2, spawn_radius*2);
    
    Vector3 start_pos;
    Vector3 direction; 
    float random_pos = 2 * spawn_radius * Random.value; 
      
    switch (Random.Range(0, 4))
    {
        case 0:
          start_pos = new Vector3(spawn_rect.xMin, 0, spawn_rect.yMax) + Vector3.back*random_pos;
          direction = Vector3.right;
          break;
        case 1:
          start_pos = new Vector3(spawn_rect.xMax, 0, spawn_rect.yMax) + Vector3.back*random_pos;
          direction = Vector3.left;
          break;
        case 2:
          start_pos = new Vector3(spawn_rect.xMin, 0, spawn_rect.yMax) + Vector3.right*random_pos;
          direction = Vector3.back;
          break;
        default :
          start_pos = new Vector3(spawn_rect.xMin, 0, spawn_rect.yMin) + Vector3.right*random_pos;
          direction = Vector3.forward;
          break;
    }
    Color color = GetRandomColor();
    Marker random_marker = CreateMarker();
    random_marker.Init(color, start_pos, direction, this);

       
    }

    private List<Color> colors = new List<Color> {Color.white, Color.red, Color.green, Color.blue};
  private Color GetRandomColor(int start_idx = 0)
  {
    return colors[Random.Range(start_idx,colors.Count)];
  }
  
  void OnDrawGizmos() {
    Gizmos.color = Color.green;
    Gizmos.DrawCube(new Vector3(spawn_rect.center.x, 0, spawn_rect.center.y), new Vector3(spawn_rect.width, 0, spawn_rect.height) );
  }

  void OnGUI() {
    if (state == GameState.DEFEAT)
        {
     GUI.BeginGroup (new Rect (Screen.width / 2 - 50, Screen.height / 2 - 50, 100, 100));
      GUI.Box (new Rect (0,0,100,100), state == GameState.VICTORY ? "VICTORY" : "DEFEAT");
      if(GUI.Button(new Rect(10, 40, 80, 30), "Play again!"))
      {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
      }
      GUI.EndGroup ();
    }
  }
  
  public static bool IsGame()
  {
    return state == GameState.GAME;
  }
  
  public static void GameOver(bool is_victory)
  {
    state =  GameState.DEFEAT;
  }

    public static void Level2(bool is__victory)
    {
        portal_animator.SetBool("open", is__victory);
     
        metka = true;
    }
   public static  bool metka = false;

   

  internal void IncreaseTime(int seconds)
  {
    start_game_stamp += seconds;
    
    lefttime_animator.SetTrigger("Shake");
  }  

}
