﻿using System.Collections.Generic;
using Base;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class game2 : MonoBehaviour
{


    public GameObject player_go;



    public GameObject camera_go;
    public GameObject vfx_step;
    public AudioClip sfx_step;
    public AudioClip ambient;
    public GameObject portal;
    public GameObject portal1;
    public GameObject marker_go;
    public GameObject babax;

    public RawImage gamepad_base_go;
    public RawImage gamepad_stick_go;

    static float start_game_stamp;
    float game_time = 20;

    float spawn_markers_stamp;
    float spawn_markers_cooldown = 0.5f;


    Player player;


    PlayerFollowing player_following;
    AudioSource ambient_source;
    public static Animator portal_animator;
    int port = 0;
    void Start()
    {

        
        
        Cube.vfx_step = vfx_step;
        Cube.sfx_step = sfx_step;



        PlayerInput input = player_go.AddComponent<PlayerInput>();
        input.Init(gamepad_base_go, gamepad_stick_go);

        player = player_go.AddComponent<Player>();
        player_following = camera_go.AddComponent<PlayerFollowing>();
        player_following.Init(player.gameObject);








        ambient_source = gameObject.AddComponent<AudioSource>();
        ambient_source.clip = ambient;
        ambient_source.loop = true;
        ambient_source.Play();


    }

    void ColorizeGo(GameObject go, Color color)
    {
        Renderer renderer = go.GetComponent<Renderer>();
        var tempMaterial = new Material(renderer.sharedMaterial);
        tempMaterial.color = color;
        tempMaterial.SetColor("_EmissionColor", Mathf.LinearToGammaSpace(0.02f) * color);
        renderer.sharedMaterial = tempMaterial;
    }





    float exit_arrow_radius = 20;
    float spawn_radius = 30;
    Rect spawn_rect = Rect.zero;
    void Update()
    {

        babax.SetActive(true);

        Marker.destroy1();
        if (port == 35)
        {
            portal1.SetActive(false);
        }
        port++;



        ambient_source.volume = player.activity;

        Vector3 player_pos = player.transform.position;

        Vector3 center_screen_pos = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2)) - player_following.offset;













        spawn_rect.position = new Vector2(player_pos.x - spawn_radius, player_pos.z - spawn_radius);
        spawn_rect.size = new Vector2(spawn_radius * 2, spawn_radius * 2);
        if (count != 40) { 
        Vector3 start_pos = new Vector3(Random.Range(10, 70), 0, Random.Range(0, 60));

        float random_pos = 2 * spawn_radius * Random.value;
        Vector3 direction = new Vector3(0, 0, 0);
        Color color = GetRandomColor();
        Marker random_marker = CreateMarker();
        random_marker.Init1(color, start_pos, direction);
            count++;
    }

    }

    int count = 0;
    private List<Color> colors = new List<Color> { Color.white, Color.green, Color.blue, Color.grey, Color.black };
    private Color GetRandomColor(int start_idx = 0)
    {
        return colors[Random.Range(start_idx, colors.Count)];
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawCube(new Vector3(spawn_rect.center.x, 0, spawn_rect.center.y), new Vector3(spawn_rect.width, 0, spawn_rect.height));
    }


    Marker CreateMarker()
    {
        GameObject marker = Instantiate(marker_go);
        marker.SetActive(true);
        return marker.AddComponent<Marker>();
    }

  









}
