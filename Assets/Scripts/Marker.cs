﻿using UnityEngine;

[RequireComponent(typeof(Cube))]
public class Marker : MonoBehaviour
{    
    internal Cube cube;
    void Awake()
    {
        cube = GetComponent<Cube>();
        
        cube.on_rotate = delegate
        {
            step_source.Play();
        };
    }

    private float max_volume = 0.3f;
    private AudioSource step_source;
    private Vector3 speed;
    private Game game;

    public void Init1(Color color, Vector3 position, Vector3 speed)
    {
       
        cube.color = color;
        transform.position = position;
        this.speed = speed;

        step_source = gameObject.AddComponent<AudioSource>();
        step_source.clip = Cube.sfx_step;
        step_source.volume = max_volume;
        step_source.pitch = 50;
    }

    public void Init(Color color, Vector3 position, Vector3 speed, Game game)
    {
        this.game = game;
        cube.color = color;
        transform.position = position;
        this.speed = speed;
        
        step_source = gameObject.AddComponent<AudioSource>();
        step_source.clip = Cube.sfx_step;
        step_source.volume = max_volume;
        step_source.pitch = 50;
    }
   public static int kill = 0;
    public static void destroy()
    {
         kill = 1;
    }
    public static void destroy1()
    {
        kill = 0;
    }
    void Update()
    {
        if (kill == 1)
        {
            Destroy(gameObject);
            return;
        }

        cube.Move(speed.normalized);

        var sound_radius = 10;
        var dist_to_player = (game.player_go.transform.position - gameObject.transform.position).magnitude;
        if(dist_to_player > sound_radius)
            step_source.volume = 0;
        else
            step_source.volume = (1 - dist_to_player / sound_radius) * max_volume;   

    }
    
    void OnTriggerEnter(Collider other)
    {
        var player = other.gameObject.GetComponent<Player>();
        if (player != null)
        {
            if (cube.color != Color.white)
            {
                player.SwallowColor(cube.color);
            }
            else
            {
                game.IncreaseTime(10);
            }

            
            

            Destroy(gameObject);
        }
    }

    void OnBecameInvisible() {
        Destroy(gameObject);
    }
}
