﻿using System;
using Base;
using UnityEngine;

[RequireComponent(typeof(Cube))]
public class Player : MonoBehaviour
{
    internal Cube cube;
    internal PlayerInput input;
    void Awake()
    {
        cube = GetComponent<Cube>();
        input = GetComponent<PlayerInput>();
        
        cube.on_rotate = delegate
        {
            step_source.Play();
        };
    }
    
    private AudioSource step_source;
    
    internal float activity;
    float activity_step = 0.01f;
    void Start()
    {
        BoxCollider collider = gameObject.GetComponent<BoxCollider>();
        collider.isTrigger = true;   
        
        Rigidbody rb = gameObject.AddComponent<Rigidbody>();
        rb.useGravity = false;
        rb.isKinematic = true;
        rb.angularDrag = 0;
        
        step_source = gameObject.AddComponent<AudioSource>();
        step_source.clip = Cube.sfx_step;
        step_source.volume = 0.3f;
    }
    
    public void Update()
    {
        cube.Move(input.axis);
        activity = Mathf.Clamp01(activity + (input.axis.magnitude > 0.1f ? activity_step : activity_step * -1));
    }

    public void SwallowColor(Color color)
    {
        Color new_color = cube.color - Color.white*0.2f + color * 0.6f;
        new_color.r = Mathf.Clamp01(new_color.r);
        new_color.g = Mathf.Clamp01(new_color.g);
        new_color.b = Mathf.Clamp01(new_color.b);
        new_color.a = 1;
        cube.color = new_color;
    }
}
